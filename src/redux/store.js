import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { ADD_TO_CART, GET_COURSE_LIST, REMOVE_FROM_CART } from './actions';

const initialStore = {
	cart: [],
};
const initialCourses = {
	courses: [],
};

const cartReducer = (state = initialStore, action) => {
	switch (action.type) {
		case ADD_TO_CART:
			let exists = state.cart.find((id) => id === action.id);

			return exists
				? { ...state }
				: { ...state, cart: state.cart.concat(action.id) };

		case REMOVE_FROM_CART:
			return {
				...state,
				cart: state.cart.filter((id) => id != action.id),
			};
		default:
			return state;
	}
};

const coursesReducer = (state = initialCourses, action) => {
	console.log(action);
	switch (action.type) {
		case GET_COURSE_LIST:
			return {
				...state,
				courses: action.courses,
			};
	}
	return state;
};
const result = createStore(
	combineReducers({ cartReducer, coursesReducer }),
	composeWithDevTools(applyMiddleware(thunk))
);
export default result;
