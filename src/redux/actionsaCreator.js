import Axios from 'axios';
import { ADD_TO_CART, GET_COURSE_LIST, REMOVE_FROM_CART } from './actions';

const addToCart = (id) => {
	return {
		type: ADD_TO_CART,
		id,
	};
};

const removeFromCart = (id) => {
	return {
		type: REMOVE_FROM_CART,
		id,
	};
};

const getCourseList = () => (dispatch) => {
	Axios.get(
		'http://my-json-server.typicode.com/betoquiroga/json-db/cursos'
	).then((res) =>
		dispatch({
			type: GET_COURSE_LIST,
			courses: res.data,
		})
	);
};

export { addToCart, removeFromCart, getCourseList };
