import React from 'react';
import { connect } from 'react-redux';
import { removeFromCart } from '../../redux/actionsaCreator';

const Cart = ({ cart, removeCourseFromCart }) => {
	return (
		<div>
			<h1>Cart</h1>
			<ul>
				{cart.map((id) => {
					return (
						<li onClick={() => removeCourseFromCart(id)} key={id}>
							{id}
						</li>
					);
				})}
			</ul>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		cart: state.cartReducer.cart,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		removeCourseFromCart(id) {
			dispatch(removeFromCart(id));
		},
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
