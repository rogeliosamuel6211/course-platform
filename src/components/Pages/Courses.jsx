import React from 'react';
import { connect } from 'react-redux';
import CourseGrid from '../Organisms/CourseGrid';

const Courses = ({ courses }) => <CourseGrid courses={courses} />;

const mapStateToProps = (state) => {
	return {
		courses: state.coursesReducer.courses,
	};
};

const mapDispatchToProps = () => {
	return {};
};

export default connect(mapStateToProps, {})(Courses);
