import React from 'react';
import '../styles/styles.scss';
import { Provider } from 'react-redux';
import result from '../redux/store';

import AppRoutes from './AppRoutes';
import { getCourseList } from '../redux/actionsaCreator';

result.dispatch(getCourseList());

const App = () => {
	return (
		<Provider store={result}>
			<AppRoutes />
		</Provider>
	);
};

export default App;
