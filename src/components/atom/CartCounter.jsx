import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const CartCounter = ({ cart }) => {
	return (
		<Link to="/cart">
			<button className="button tiny ghost">Carrito: {cart.length}</button>
		</Link>
	);
};

//read the global state and convert it to props for this component
//This works because we pass these two cb as a parameter of connect function
const mapStateToProps = (state) => {
	console.log(state);
	return {
		cart: state.cartReducer.cart,
	};
};

const mapDispatchToProps = () => {
	return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(CartCounter);
